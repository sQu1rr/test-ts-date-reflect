import { test, Class } from './test';

import {} from 'mocha';
import { should } from 'chai';

should();

class Test {
    @test item: String;
    @test date: Date;
    @test instance: Class;
}

describe('dummy', () => { it('dummy', () => { (1 + 1).should.equal(2); }); });
