var webpackConfig = require('./webpack.test');

module.exports = function (config) {
    var _config = {
        basePath: '',

        frameworks: [ 'mocha' ],

        plugins: [ 'karma-mocha', 'karma-webpack', 'karma-sourcemap-loader',
                   'karma-phantomjs-launcher', 'karma-mocha-reporter' ],

        files: [
            {pattern: './karma-test-shim.js', watched: false}
        ],

        preprocessors: {
            './karma-test-shim.js': [ 'webpack', 'sourcemap' ]
        },

        webpack: webpackConfig,

        webpackMiddleware: {
            stats: 'errors-only'
        },

        webpackServer: {
            noInfo: true
        },

        reporters: [ 'mocha' ],
        port: 9876,
        colors: true,
        logLevel: config.LOG_DEBUG,
        autoWatch: false,
        browsers: [ 'PhantomJS' ],
        singleRun: true
    };

    config.set(_config);
};
