## Running with docker

```bash
docker build . -t test
docker run --rm test ts-mocha test.ts # simple mocha test (wrong type)
docker run --rm test npm run test # angular karma test (correct type)
```
