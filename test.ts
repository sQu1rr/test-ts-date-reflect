// empty import to calm compiler down
import {} from 'reflect-metadata';
import 'core-js/es7/reflect';

export function test(target: any, key: string) {
    console.log(key, Reflect.getMetadata('design:type', target, key).name);
}

export class Class { }

class Test {
    @test item: String;
    @test date: Date;
    @test instance: Class;
}
