FROM node

WORKDIR /usr/src/app

RUN npm i -g typescript
RUN npm i -g mocha
RUN npm i -g ts-mocha

ADD package.json /usr/src/app
RUN npm i

ADD . /usr/src/app
